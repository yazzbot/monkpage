import "../../css/ErrorMessages.css";
import notFound from "../../resources/img/icons/not_found.png";
import React from 'react';

/**
 * Error message displayed when no products are found
 * @returns {JSX.Element}
 * @constructor
 */
export default function EmptyDbError() {

    return <div id="empty_db-error_container" align="center">
        <img src={notFound} height="145px" width="145px" alt="Something went wrong."/>
        <p id="empty-db-message">Could not find any products...</p>
    </div>
}