import notFound from "../../resources/img/icons/not_found.png";
import "../../css/ErrorMessages.css";
import {Link} from "react-router-dom";
import React from 'react';

/**
 * Error message displayed whenever an invalid url is reached.
 *
 * @returns {JSX.Element}
 * @constructor
 */
export default function ProductPage() {

    return <div id="not-found_container" align="center">
        <img src={notFound} height="150px" width="150px" alt="Something went wrong."/>
        <p id="not-found-message">Uh-oh... <br/>
            Something went wrong <br/>
        </p>
        <Link to={"/"}>
            <button id="frontpage-button">Go to frontpage</button>
        </Link>
    </div>
}