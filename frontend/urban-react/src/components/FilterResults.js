import {useParams} from "react-router-dom";
import ProductGrid from "./product-view/ProductGrid";
import {ProductFilter} from "./product-view/ProductFilter";
import React from 'react';

const productsURL = "?category="

/**
 * Represents the view of products after filtering.
 *
 * @returns {JSX.Element}
 * @constructor
 */
export default function FilterResults() {
    const keyWord = useParams().keyWord;

    return <main>
        <ProductFilter />
        <section id="products_container">
            <ProductGrid productsURL={productsURL + keyWord}/>
        </section>
    </main>
}