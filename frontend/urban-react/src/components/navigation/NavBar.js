import React from "react";
import {Link} from "react-router-dom";
import "../../css/navigation/TopNav.css";
import "../../css/Font.css";
import logo from "../../resources/img/logo/LOGO_GREEN/newlogo2_green.jpg";
import user from "../../resources/img/icons/account_icon.png";
import cart from "../../resources/img/icons/cart_icon.png";
import SearchBarNav from "./SearchBarNav";

/**
 * Represents a top navigation bar.
 *
 * @param props
 * @returns {JSX.Element}
 * @constructor
 */
export function NavBar(props) {
    let accountCaption;
    let accountNav;

    if (props.user) {
        accountCaption = props.user.username;
        accountNav = "/profile-page";
    } else {
        accountCaption = "Sign in";
        accountNav = "/login";
    }

    return (
        <>
            <nav className="top-nav">

                <div id={"mobile-view-wrap"}>
                    <a href="/">
                        <img id="logo-image" src={logo} alt="LOGO"/>
                    </a>
                    <div className="menu-bar">
                        <Link to={"/products"} className="list-menu" id={"products"}>
                            <h4 className="menu-header">Products</h4>
                        </Link>
                        <Link to={"/InProgressSite"} className="list-menu" id={"discover"}>
                            <h4 className="menu-header">Discover</h4>
                        </Link>
                        <Link to={"/InProgressSite"} className="list-menu" id={"aboutUs"}>
                            <h4 className="menu-header">About us</h4>
                        </Link>
                    </div>
                </div>

                <div className="icon-bar">
                    <div className="icon-selection" id={"searchBox"}>
                        <SearchBarNav/>
                    </div>

                    <Link to={accountNav} user={props.user} className="icon-selection" id={"userBox"}>
                        <img src={user} alt="Account icon"/>
                        <h5 className="icon-caption">{accountCaption}</h5>
                    </Link>

                    <Link to={"/cart"} className="icon-selection">
                        <img src={cart} alt="Cart icon"/>
                        <h5 className="icon-caption">Cart</h5>
                    </Link>
                </div>
            </nav>
        </>
    )
}