import React from "react"; //Is it necessary to include this line in every ReactJS-files?
import {Link} from "react-router-dom";
import "../../css/navigation/Footer.css";
import "../../css/Font.css";
import yogiTea from "../../resources/img/yogi_tea_logo.png";
import ahmadTea from "../../resources/img/ahmad_tea_logo.png";
import clipperTea from "../../resources/img/clipper_tea_logo.png";
import pukkaTea from "../../resources/img/pukka_tea_logo.png";
import fairTrade from "../../resources/img/fair_trade_logo.png";

/**
 * Represents a footer navigation bar.
 * @returns {JSX.Element}
 * @constructor
 */
export const Footer = () => {
    return (
        <footer>
            <nav className="bottom-nav">
                <div id="footer-shop">
                    <h3>Shop:</h3>
                    <Link to={"/InProgressSite"}>
                        <h4>Vitamins</h4>
                    </Link>
                    <Link to={"/InProgressSite"}>
                        <h4>Herbal Tea</h4>
                    </Link>
                    <Link to={"/InProgressSite"}>
                        <h4>Green Tea</h4>
                    </Link>
                    <Link to={"/InProgressSite"}>
                        <h4>Black Tea</h4>
                    </Link>
                </div>

                <div id="footer-info">
                    <h3>About us:</h3>
                    <Link to={"/InProgressSite"}>
                        <h4>History of Tea</h4>
                    </Link>
                    <Link to={"/InProgressSite"}>
                        <h4>Water temperature</h4>
                    </Link>
                    <Link to={"/InProgressSite"}>
                        <h4>Whole sale</h4>
                    </Link>
                    <Link to={"/InProgressSite"}>
                        <h4>Contacts</h4>
                    </Link>
                </div>

                <div id="footer-partners">
                    <h3>Partners:</h3>
                    {/*TODO: Find better logos for this*/}

                    {/*Logos of company Urban Infusion cooperate with*/}

                    {/*Yogi tea*/}
                    <div className="partnerImg">
                        <img src={yogiTea} alt={"Yogi Tea"} onClick={() => YogiTea()}/>
                    </div>
                    {/*<Link to={"/yogi_tea"} target="_blank" element={}>*/}
                    {/*    <img src={yogiTea} alt={"Yogi Tea"}/>*/}
                    {/*</Link>*/}

                    {/*Ahmad Tea*/}
                    <div className="partnerImg">
                        <img src={ahmadTea} alt={"Ahmad Tea"} onClick={() => AhmadTea()}/>
                    </div>

                    {/*Clipper Tea*/}
                    <div className="partnerImg">
                        <img src={clipperTea} alt={"Clipper Tea"} onClick={() => ClipperTea()}/>
                    </div>

                    {/*Pukka Tea*/}
                    <div className="partnerImg">
                        <img src={pukkaTea} alt={"Pukka Tea"} onClick={() => PukkaTea()}/>
                    </div>

                    {/*Fair Trade*/}
                    <div className="partnerImg">
                        <img src={fairTrade} alt={"Fair Trade"} onClick={() => FairTrade()}/>
                    </div>

                </div>
            </nav>
            <p id={"notRealPage"}><small>For educational purpose </small></p>

        </footer>
    )

}

const YogiTea = () => {
    window.location.href = "https://www.yogitea.com/en/"
    return null;
}

const AhmadTea = () => {
    window.location.href = "https://www.ahmadtea.com/"
    return null;
}

const ClipperTea = () => {
    window.location.href = "https://www.clipper-teas.com/"
    return null;
}

const PukkaTea = () => {
    window.location.href = "https://www.pukkaherbs.com"
    return null;
}

const FairTrade = () => {
    window.location.href = "https://www.fairtrade.net/"
}
