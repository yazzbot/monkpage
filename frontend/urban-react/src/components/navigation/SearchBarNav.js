import React from "react";
import {useState} from "react";
import "../../css/navigation/Search.css";
import "../../css/Font.css";
import search from "../../resources/img/icons/search_icon.png";

/**
 * Represents a search bar, used in top navigation bar.
 *
 * @returns {JSX.Element}
 * @constructor
 */
export default function SearchBarNav(){
    const [searchField,setSearchField] = useState("")

    return(
        <div className="searchField">
            <div className="searchBar">
                <input className="inputField"
                       placeholder="Search..."
                       onChange={(e)=>setSearchField(e.target.value)}/>
                <div onClick={event =>window.location="/search/"+searchField} className="search-button">
                    <img src={search} alt="Search icon"/>
                    <h5 className="icon-caption">Search </h5>
                </div>
            </div>
        </div>
    )

}

