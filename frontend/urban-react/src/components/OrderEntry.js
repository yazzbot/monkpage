import React from "react";
import OrderItems from "./OrderItems";

/**
 * Represents an entry in the 'your orders' page
 *
 * @param props
 * @returns {JSX.Element}
 * @constructor
 */
export function OrderEntry(props) {

    return <>
        <div className="order-details">
            <h4 className="order-font">Order {props.order.id}</h4>
            <h4 className="order-font">{props.order.createdDate}</h4>
        </div>
        <div className="ordered-products">
            <OrderItems items={props.items.orderItems} key={props.order.id}/>
            <div className="order-total-price_container">
                <h4 className="order-font">Total</h4>
                <h4 className="total-price">{props.items.totalPrice},-</h4>
            </div>
        </div>

    </>
}