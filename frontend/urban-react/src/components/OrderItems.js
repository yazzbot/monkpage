import React from "react";

/**
 * Represents the ordered items displayed in 'your orders' page
 *
 * @param props
 * @returns {JSX.Element}
 * @constructor
 */
export default function OrderItems(props) {

    return <>
            {props.items.map((item) => {
                return <div className="ordered-items-details">
                    <div className="order-quantity_container">
                        <h4 className="order-font">{item.quantity}x</h4>
                    </div>
                    <div className="order-title_container">
                        <h4 className="order-font">{item.product.title}</h4>
                    </div>
                    <div className="order-price_container">
                        <h4 className="order-font">{item.product.price * item.quantity},-</h4>
                    </div>
                </div>
            })
            }
    </>

}