import React from "react";
import {useNavigate} from "react-router-dom";
import "../css/InProgressSIte.css";
import "../css/Font.css";

/**
 * In progress site, used whenever a page is not yet implemented.
 *
 * @returns {JSX.Element}
 * @constructor
 */
export const InProgressSite = ()=>{
    const history = useNavigate();
    return(
        <div className="wrapper">
            <div id="content">
                <h1>Page still in progress</h1>
                <div id="loader">
                </div>
                <button id="mainSite" onClick={()=>history("/")}>Main Site</button>
            </div>



        </div>

)
}