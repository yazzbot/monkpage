import {useState} from "react";
import {EditButton} from "../buttons/EditButton";
import {SaveButton} from "../buttons/SaveButton";
import {DeleteButton} from "../buttons/DeleteButton";
import React from 'react';
import {Link} from "react-router-dom";

/**
 * Represents a product page.
 *
 * @param props
 * @returns {JSX.Element}
 * @constructor
 */
export default function ProductPageView(props) {
    const [title, setTitle] = useState(props.product.title);
    const [price, setPrice] = useState(props.product.price);
    const [summary, setSummary] = useState(props.product.summary);
    const [sku, setSku] = useState(props.product.sku);
    const [discount,] = useState(props.product.discount);
    const [quantity] = useState(props.product.quantity);
    const [createdAt] = useState(props.product.createdAt);
    const [updatedAt] = useState(props.product.updatedAt);
    const [imageURL, setImageURL] = useState(props.product.imageURL);

    const [count, setCount] = useState(1);

    const [editMode, setEditMode] = useState(false);

    let titleElement;
    let priceElement;
    let summaryElement;
    let skuElement;
    let imageUrlElement;
    let purchaseOptions;
    let editButton;
    let deleteButton;

    let returnContent;

    // Increase quantity
    const addCountHandler = () => {
        if (count < sku) {
            setCount(count + 1);
            console.log(imageURL);
        }
    };

    //Decrease quantity
    const removeCountHandler = () => {
        if (count === 1) {
            return;
        }
        setCount(count - 1);
    };

    // Set to edit view if edit mode is enabled
    if (editMode) {
        titleElement = <div className="product-card-editable">
            <label htmlFor="title">Title:</label>
            <input type="text" id="title-input" placeholder="Title" value={title}
                   onChange={(event) => setTitle(event.target.value)}/>
        </div>;
        priceElement = <div className="product-card-editable">
            <label htmlFor="price">Price:</label>
            <input type="text" id="price-input" placeholder="Price" value={price}
                   onChange={(event) => setPrice(event.target.value)}/>
        </div>;
        summaryElement = <div className="product-card-editable">
            <label htmlFor="summary">Summary:</label>
            <input type="text" id="summary-input" placeholder="Summary" value={summary}
                   onChange={(event) => setSummary(event.target.value)}/>
        </div>;
        skuElement = <div className="product-card-editable">
            <label htmlFor="sku">Available quantity:</label>
            <input type="text" id="sku-input" placeholder="Available quantity" value={sku}
                   onChange={(event) => setSku(event.target.value)}/>
        </div>;
        imageUrlElement = <div className="product-card-editable">
            <label htmlFor="image-url">Image URL:</label>
            <input type="text" id="image-url-input" placeholder="Image URL" value={imageURL}
                   onChange={(event) => setImageURL(event.target.value)}/>
        </div>;
        purchaseOptions = null;
        // Check for input values, hide save button if no input
        const haveValues = title !== "" && price > 0 && summary !== "";
        if (haveValues) {
            editButton = <SaveButton clickFunction={saveProduct}/>;
        }
    } else {
        // Set to user view if edit mode is not enabled
        titleElement = <h1 id="pp-title">{title}</h1>;
        priceElement = <h3 id="pp-price">{count * price},-</h3>;
        summaryElement = <h4 id="pp-summary">{summary}</h4>;
        purchaseOptions = <div id="buy-options_container">
            <div className="pp-counter">
                <button onClick={removeCountHandler} className="pp-count-btn">-</button>
                <div className="count">{count}</div>
                <button onClick={addCountHandler} className="pp-count-btn">+</button>
            </div>
            <Link to={"/cart"}>
                <button className="pp-buy-button">ADD TO CART</button>
            </Link>
        </div>;

        skuElement = null;
        imageUrlElement = null;
        editButton = <EditButton clickFunction={toggleEditing}/>;
        deleteButton = <DeleteButton clickFunction={deleteProduct} />;
    }

    // Set return content
    returnContent = <>
        <img id="pp-image" src={props.product.imageURL} alt="Product"/>
        <div id="product-details_container">
            {titleElement}
            {priceElement}
            {summaryElement}
            {purchaseOptions}
            {skuElement}
            {imageUrlElement}
        </div>
    </>

    if (props.user) {
        if (props.user.username === "admin") {
            return <>
                {returnContent}
                <div className="admin-product-buttons">
                    {editButton}
                    {deleteButton}
                </div>
            </>
        }
    }
    return <>
        {returnContent}
        </>


    /**
     * Toggle the "Edit mode"
     */
    function toggleEditing() {
        setEditMode(!editMode);
    }

    /**
     * Save the product
     */
    function saveProduct() {
        const product = {
            "id": props.product.id,
            "title": title,
            "summary": summary,
            "sku": sku,
            "price": price,
            "discount": discount,
            "quantity": quantity,
            "createdAt": createdAt,
            "updatedAt": updatedAt,
            "imageURL": imageURL
        };
        if (props.product.id) {
            props.saveFunction(product);
        }
        toggleEditing();
    }

    /**
     * This function is called when the user clicks on "Delete"
     */
    function deleteProduct() {
        // Call the deletion-function, received from the parent component
        props.deleteFunction(props.product);
    }
}