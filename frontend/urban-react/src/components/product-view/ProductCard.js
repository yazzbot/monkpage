import {Link} from "react-router-dom";
import "../../css/product-view/ProductCard.css";
import "../../css/Font.css";
import React from "react";

/**
 * A component representing a single product card
 * @param props Properties containing the following:
 *  - product - the product to edit
 * @return {JSX.Element}
 * @constructor
 */
export function ProductCard(props) {

    return <div className="product-card">
        <Link to={"/products/" + props.product.slug} className="product-card-link">
            <div className="product-card-image">
                <img id={"product-img"} src={props.product.imageURL} alt="product"/>
            </div>
            <h4 className="product-card-title">{props.product.title}</h4>
            <h5 className="product-card-price">{props.product.price} kr</h5>
            <div className="product-card-description">{props.product.description}</div>
        </Link>
        <Link to={"/cart"}>
            <button className="button"> Add to cart </button>
        </Link>
    </div>

}
