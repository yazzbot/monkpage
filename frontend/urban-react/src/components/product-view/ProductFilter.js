import React from "react";

/**
 * Represents a product filter bar, used on product catalog.
 *
 * @returns {JSX.Element}
 * @constructor
 */
export function ProductFilter(){

    return(
        <section id="product-filter-bar">
            <h3 className="filter-header">Tea</h3>
            <ul id="tea-filter">
                <li>
                    <input onClick={event =>window.location="/products/filter/category=herbal-tea"} type="checkbox" id="herbal-tea" name="herbal-tea"/>
                        <label htmlFor="herbal-tea">Herbal tea</label>
                </li>
                <li>
                    <input onClick={event =>window.location="/products/filter/category=black-tea"} type="checkbox" id="black-tea" name="black-tea"/>
                        <label htmlFor="black-tea">Black tea</label>
                </li>
                <li>
                    <input onClick={event =>window.location="/products/filter/category=green-tea"} type="checkbox" id="green-tea" name="green-tea"/>
                        <label htmlFor="green-tea">Green tea</label>
                </li>
                <li>
                    <input onClick={event =>window.location="/products/filter/category=fruit-tea"} type="checkbox" id="fruit-tea" name="fruit-tea"/>
                        <label htmlFor="fruit-tea">Fruit tea</label>
                </li>

            </ul>
            <h3 className="filter-header">Accessories</h3>
            <ul id="merch-filter">
                <li>
                    <input onClick={event =>window.location="/products/filter/category=tea-mug"} type="checkbox" id="mugs" name="mugs" />
                        <label htmlFor="tea-mugs">Mugs</label>
                </li>
                <button style={{
                    backgroundColor:"#063A08",
                    color: "#ffffff",
                    padding: "0.2rem 0.2rem",
                    borderRadius: "6px",
                    borderStyle: "none",
                    fontSize: "1rem",
                    fontWeight: "200",
                    cursor: "pointer",
                    minWidth: "0.5rem",
                    width: "6rem",
                }} onClick={event => window.location="/products"} type="reset" id="reset-button" name="reset-button">
                    Reset filters
                </button>
            </ul>

    </section>
    )
}