import "../../css/frontpage/Testimonials.css";
import "../../css/Font.css";
import React from 'react';

/**
 * Represents the testimonials section, used on frontpage.
 *
 * @returns {JSX.Element}
 * @constructor
 */
export default function Testimonials() {

    return <>
        <div id="testimonials_wrapper">
            <h2 id="test-header" align="center">What our customers think</h2>
            <div className="testcard">
                <div className="testcard-thumb">
                    <img
                        src="https://alchetron.com/cdn/john-cliff-1c1b1c38-686a-4940-b9dd-57aa792926f-resize-750.jpeg"
                        className="client-img"
                        alt=""
                    />
                    <span className="client-name"> John Cliff</span>
                </div>

                <div className="testcard-body">
                    <p className="review">
                        "Tea is the necessary component for my busy days at the office.
                        It helps me focus. Urban Infusion have the best Sencha tea <br/> – my
                        favorite. "
                    </p>
                </div>
            </div>

            <div className="testcard-middle">
                <div className="testcard-body">
                    <p className="review">
                        "I love the great selection Urban Infusion have! I try a new tea
                        every day and I still have many teas to try out! Recommend!"
                    </p>
                </div>

                <div className="testcard-thumb-middle">
                    <img
                        src="https://m.media-amazon.com/images/I/71RnD+IM7nL._AC_SL1200_.jpg"
                        className="client-img"
                        alt=""
                    />
                    <span className="client-name-middle"> Purple Floyd</span>
                </div>
            </div>

            <div className="testcard">
                <div className="testcard-thumb">
                    <img
                        src="https://d.ibtimes.co.uk/en/full/1510186/james-jagger-attends-new-york-premiere-vinyl.jpg"
                        className="client-img"
                        alt=""
                    />
                    <span className="client-name"> James Jagger</span>
                </div>

                <div className="testcard-body">
                    <p className="review">
                        "Man, their teas are dope! P.S. You can also smoke them, just
                        don't tell anyone I told you that"
                    </p>
                </div>
            </div>
        </div>
    </>
}