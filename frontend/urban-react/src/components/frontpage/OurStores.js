import {Link} from "react-router-dom";
import "../../css/frontpage/OurStores.css";
import "../../css/Font.css";
import React from 'react';

/**
 * Represents the company presentation section, used on frontpage.
 *
 * @returns {JSX.Element}
 * @constructor
 */
export default function OurStores() {

    return (
        <section className="our-stores-section">
            <div className="our-stores-wrapper">
                <h2 id="our-stores-header">Urban Infusion</h2>
                <div className="text-wrapper">
                    <div className="our-stores-text">
                        <p>
                            We have carefully created a collection of more than 600
                            different teas from <br/>140 different countries. Come visit us at
                            our office in Amfi Moa, we have <br/> 62 most popular teas available
                            here!  <br/> Our shop is mask free (just don't tell it to municipality's
                            chief physician!)
                        </p>
                    </div>
                </div>

                <div className="buttons-wrapper">
                    <Link to={"/InProgressSite"}>
                        <button className="our-stores-button">Our Stores</button>
                    </Link>
                    <Link to={"/InProgressSite"}>
                        <button className="our-stores-button">Collections</button>
                    </Link>
                    <Link to={"/InProgressSite"}>
                        <button className="our-stores-button">History of tea</button>
                    </Link>
                </div>
                <iframe id="maps-window" title="google-maps"
    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1844.3320659373792!2d6.3478628165093705!3d62.46698428286438!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4616c5a0d71b4859%3A0xd170bc14616783ee!2sAMFI%20Moa!5e0!3m2!1sno!2sno!4v1654598162425!5m2!1sno!2sno"
    width="1400" height="500" allowFullScreen="" loading="lazy"
    referrerPolicy="no-referrer-when-downgrade"/>
            </div>
        </section>);
}