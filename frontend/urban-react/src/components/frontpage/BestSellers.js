import "../../css/ErrorMessages.css";
import "../../css/Font.css";
import ProductGrid from "../product-view/ProductGrid";
import React from 'react';

/**
 * Represents a best seller section, used on front page.
 *
 * @returns {JSX.Element}
 * @constructor
 */
export default function BestSellers() {
    const productsURL = "/best-sellers";

    return <section className="best-sellers">
        <div id="best-sellers-box">
            <h2 id="best-sellers-header" align="center">Featured</h2>
            <div id="featured-cards">
                <ProductGrid productsURL={productsURL}/>
            </div>
        </div>
    </section>

}