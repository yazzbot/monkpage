import {Link} from "react-router-dom";
import "../../css/frontpage/HeroSection.css";
import "../../css/Font.css";
import React from 'react';

/**
 * Represents a hero section, used on front page.
 *
 * @returns {JSX.Element}
 * @constructor
 */
export default function HeroSection() {

    return <section className="hero-section">
        <Link to={"/products"}>
            <button className="hero-button">Find your herbal friend!</button>
        </Link>
    </section>
}