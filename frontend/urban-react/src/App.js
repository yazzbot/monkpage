import './App.css';
import {SignUp} from "./pages/login/SignUp";
import {Routes, Route} from "react-router-dom";
import {Home} from "./pages/Home"
import {Login} from "./pages/login/Login";
import {NavBar} from "./components/navigation/NavBar";
import ProductCatalog from "./pages/ProductCatalog";
import {Footer} from "./components/navigation/Footer";
import {InProgressSite} from "./components/InProgressSite";
import {Checkout} from "./pages/Checkout";
import ProductPage from "./pages/ProductPage";
import NotFoundError from "./components/errors/NotFoundError";
import {ConfirmOrder} from "./pages/ConfirmOrder";
import {useEffect, useState} from "react";
import {getAuthenticatedUser} from "./tools/authentication";
import ProfilePage from "./pages/ProfilePage";
import SearchResult from "./pages/SearchResult";
import FilterResults from "./components/FilterResults";
import NewProduct from "./pages/NewProduct";
import OrderHistory from "./pages/OrderHistory";
import React from 'react';



export function App() {
    const [user, setUser] = useState(null);

    useEffect(tryRestoreUserSession);

    return (
        <div>
            <NavBar user={user}/>
            <Routes>
                <Route path="/" element={<Home/>}/>
                <Route path="/products" element={<ProductCatalog user={user}/>}/>
                <Route path="/products/:productSlug" element={<ProductPage user={user}/>}/>
                <Route path="/signup" element={<SignUp/>}/>
                <Route path="/login" element={<Login setUser={setUser}/>}/>
                <Route path="/search/:keyWord" element={<SearchResult/>}/>
                <Route path="/InProgressSite" element={<InProgressSite/>}/>
                <Route path="/cart" element={<Checkout/>}/>
                <Route path="*" element={<NotFoundError/>}/>
                <Route path="/confirmOrder" element={<ConfirmOrder/>}/>
                <Route path="/profile-page" element={<ProfilePage user={user} setUser={setUser}/>}/>
                <Route path="/products/filter/category=:keyWord" element={<FilterResults/>}/>
                <Route path="/new-product" element={<NewProduct user={user} />}/>
                <Route path="/order-history" element={<OrderHistory user={user} />}/>
            </Routes>
            <Footer/>

        </div>
    );

    /**
     * Check cookies - is user logged in? If so, set the user from cookies
     */
    function tryRestoreUserSession() {
        if (!user) {
            const loggedInUser = getAuthenticatedUser();
            if (loggedInUser) {
                console.log("User session found in cookies, restoring");
                setUser(loggedInUser);
            }
        }
    }
}

export default App;
