import {sendApiDeleteRequest, sendApiPutRequest} from "../tools/requests";

const productsURL = "/products";

/**
 * Send request to the server - save product
 * @param product The product to save
 * @param callback Function to call on success
 * @param errorCallback Function to call on error
 */
export function updateProductOnServer(product) {
    sendApiPutRequest(productsURL + "/" + product.id, product);
}

/**
 * Send request to the server - delete a product
 * @param productId ID of the product to delete
 * @param callback Function to call on success
 * @param errorCallback Function to call on error
 */
export function deleteProductOnServer(productId, callback, errorCallback) {
    sendApiDeleteRequest(productsURL + "/" + productId, callback, errorCallback);
}