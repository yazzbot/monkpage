import ProductGrid from "../components/product-view/ProductGrid";
import {useParams} from "react-router-dom";
import React from 'react';

const productsURL = "/search/"

/**
 * Represents the page displaying products after search.
 *
 * @returns {JSX.Element}
 * @constructor
 */
export default function SearchResult() {
    const keyWord = useParams().keyWord;

    return <section id="products_container">
        <ProductGrid productsURL={productsURL + keyWord}/>
    </section>
}