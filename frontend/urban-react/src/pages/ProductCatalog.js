import {ProductFilter} from "../components/product-view/ProductFilter";
import '../css/product-view/ProductCatalog.css';
import Gototop from "../tools/gototop";
import "../css/ErrorMessages.css";
import ProductGrid from "../components/product-view/ProductGrid";
import {AddProductButton} from "../components/buttons/AddProductButton";
import '../css/NewProductButton.css';
import React from 'react';

/**
 * Represents the product catalog
 *
 * @param props
 * @returns {JSX.Element}
 * @constructor
 */
export default function ProductCatalog(props) {
    const productsURL = "/";
    let addProductButton;

    function handleFilters(filters, tags) {
        console.log(filters)
    }

    if (props.user) {
        if (props.user.username === "admin") {
            addProductButton = <AddProductButton clickFunction={onAddProductClick}/>;
        } else {
            addProductButton = null;
        }
    }

    return (
        <main>
            <ProductFilter
                handleFilters={filters => handleFilters(filters, "tags")}
            />
            <section id="products-catalog">
                {addProductButton}
                <div id="products_container">
                    <ProductGrid productsURL={productsURL}/>
                </div>
            </section>

            <Gototop/>
        </main>
    )

    /**
     * Show the form for adding a new product
     */
    function onAddProductClick() {
        window.location = "/new-product";
    }

}

