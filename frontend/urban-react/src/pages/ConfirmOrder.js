import React from "react";
import "../css/confirmOrder.css";
import {Link} from "react-router-dom";

/**
 * Represents a confirm order page.
 *
 * @returns {JSX.Element}
 * @constructor
 */
export function ConfirmOrder() {
    function makeId(length) {
        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        const charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() *
                charactersLength));
        }
        return result;
    }

    return (
        <div className="confirm-order-container">
            <div className="confirm-order-content">
                <h1>Thank you for your order!</h1>
                <p> Your reference code is: {makeId(10)}</p>
                <Link to="/">
                    <button className="Button">home page</button>
                </Link>
            </div>
        </div>
    )
}