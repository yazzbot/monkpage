import React, {Component} from "react";
import {OrderEntry} from "../components/OrderEntry";
import axios from "axios";
import "../css/OrderHistory.css";


let baseURL = "https://localhost:8443";

/**
 * Represents the 'your orders' page.
 */
export default class OrderHistory extends Component {

    state = {
        orders: []
    }

    // Get order data, called after render() has been invoked
    componentDidMount() {
        axios.get(baseURL + "/orders")
            .then(result => {
                const orders = result.data;
                this.setState({orders});
            }).catch(function (error) {
            console.log(error)
        });
    }

    render() {
        return <>
            <section id="order-history">
                <h2 align="center">Your orders</h2>
                {this.state.orders.map(order => {
                    return <div className="orders_container">
                        <OrderEntry order={order} key={order.id} items={order}/>
                    </div>
                })}
            </section>
        </>
    }
}