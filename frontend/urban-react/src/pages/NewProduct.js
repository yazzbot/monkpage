import {useState} from "react";
import "../css/NewProduct.css";
import {sendApiRequest} from "../tools/requests";
import {useNavigate} from "react-router-dom";
import "../css/ErrorMessages.css";
import React from 'react';

/**
 * Represents the page for creating a new product. Only accessible for admin
 *
 * @param props
 * @returns {JSX.Element}
 * @constructor
 */
export default function NewProduct(props) {
    const [title, setTitle] = useState("");
    const [summary, setSummary] = useState("");
    const [sku, setSku] = useState("");
    const [price, setPrice] = useState("");
    const [quantity] = useState(1);
    const [imageURL, setImageURL] = useState("");
    const navigate = useNavigate();
    const [error, setError] = useState("");

    let errorMessage = null;
    if (error) {
        errorMessage = <p className="error">{error}</p>;
    }

    if (props.user) {
        if (props.user.username === "admin") {
            return <section id="create-product-section">
                <h2>New product</h2>
                <form id="product-details">
                    <input className="input-field" type="text" id="title" name="title" placeholder="Title"
                           onChange={event => setTitle(event.target.value)}/>
                    <input className="input-field" type="summary" id="summary" name="summary" placeholder="Summary"
                           onChange={event => setSummary(event.target.value)}/>
                    <input className="input-field" type="sku" id="sku" name="sku"
                           placeholder="Available in stock"
                           onChange={event => setSku(event.target.value)}/>
                    <input className="input-field" type="price" id="price" name="price"
                           placeholder="Price"
                           onChange={event => setPrice(event.target.value)}/>
                    <input className="input-field" type="imageURL" id="imageURL" name="imageURL"
                           placeholder="Image URL"
                           onChange={event => setImageURL(event.target.value)}/>
                    {errorMessage}
                    <input id="submit-button" onClick={createProduct} type="submit" value="Create product"/>
                </form>
            </section>
        } else {
        }
    }
    return <h5 id="permission-denied" align="center">
        You do not have permission to enter this site.
    </h5>

    /**
     * Submit the sign-up form
     * @param event
     */
    function createProduct(event) {
        event.preventDefault();
        if (title !== "" && summary !== "" && sku !== "" && price !== "") {
            const signupData = {
                "title": title,
                "summary": summary,
                "sku": sku,
                "price": price,
                "quantity": quantity,
                "imageURL": imageURL
            };
            sendApiRequest("POST", "/products", onSignupSuccess, signupData, errorMessage => setError(errorMessage));
        } else {
            setError("Input fields cannot be empty!");
        }
    }

    /**
     * This function is called when signup was successful
     */
    function onSignupSuccess() {
        navigate("/products");
    }


}