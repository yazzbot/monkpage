import {useEffect, useState} from "react";
import axios from "axios";
import {useParams} from "react-router-dom";
import '../css/product-view/ProductPage.css';
import Gototop from "../tools/gototop";
import NotFound from "../components/errors/NotFoundError";
import ProductPageView from "../components/product-view/ProductPageView";
import {deleteProductOnServer, updateProductOnServer} from "../services/product-service";
import React from 'react';

// Base API URL
const baseURL = 'https://localhost:8443';

/**
 * Represents a product page
 *
 * @param props
 * @returns {JSX.Element}
 * @constructor
 */
export default function ProductPage(props) {
    const [product, setProduct] = useState(null);
    const slug = useParams().productSlug;
    let content;

    // Fetch data from backend, set product data to variable
    useEffect(() => {
        axios.get(baseURL + "/products/tea/" + slug)
            .then((response) => {
                setProduct(response.data);
            });
    }, [slug]);

    // Display error message if wrong url
    if (!props.product) content = <NotFound/>;

    if (product != null) {
        content = <>
            <section id="pp-details">
                <ProductPageView
                    product={product}
                    user={props.user}
                    saveFunction={saveProduct}
                    deleteFunction={deleteProduct}
                />
            </section>
            <Gototop/>
        </>
    }

    return content;

    /**
     * Send request to server to save data for a product
     * @param product The product to save
     */
    function saveProduct(product) {
        console.log(`Saving product...`);
        updateProductOnServer(product);
    }

    /**
     * Delete all information related to a specific product from the server
     * @param product
     */
    function deleteProduct(product) {
        if (product) {
            console.log("Deleting product...")
            deleteProductOnServer(product.id);
            window.location = "/";
        }
    }
}




