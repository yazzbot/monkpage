import React, {useState} from "react";
import "../css/Checkout.css";
import Gototop from "../tools/gototop";
import {Link} from "react-router-dom";

/**
 * Represents a checkout page
 * @returns {JSX.Element}
 * @constructor
 */
export function Checkout() {

    const [count, setCount] = useState(1);

    const addCountHandler = () => {
        setCount(count + 1);
    };

    const removeCountHandler = () => {
        if (count === 1) {
            return;
        }
        setCount(count - 1);
    };

    return (
        <section className="Cart-Container">
            <div className="Cart-content">
                <div className="Header">
                    <h3 className="Heading">Shopping Cart</h3>
                </div>
                <div className="Cart-Items">
                    <div className="image-box">
                        <img src="https://picsum.photos/200/300?param=1" alt="item"/>
                    </div>
                    <div className="about">
                        <h1 className="title">Black tea</h1>
                        <h3 className="subtitle">2ml</h3>
                    </div>
                    <div className="counter">
                        <button onClick={removeCountHandler} className="btn">-</button>
                        <div className="count">{count}</div>
                        <button onClick={addCountHandler} className="btn">+</button>
                    </div>
                    <div className="prices">
                        <div className="amount">249 NOK</div>
                    </div>
                </div>
                <div className="checkout">
                    <div className="total">
                        <div>
                            <div className={"Subtotal"}>Sub total</div>
                            <div className="items">{count} items</div>
                        </div>
                        <div className="total-amount">{count * 249} NOK</div>
                    </div>
                    <Link to="/confirmOrder">
                        <button className="button">Checkout</button>
                    </Link>

                </div>
            </div>
            <Gototop/>
        </section>
    )
}