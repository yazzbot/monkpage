import {useState} from "react";
import "../../css/Register.css";
import {useNavigate} from "react-router-dom";
import {sendAuthenticationRequest} from "../../tools/authentication";
import React from 'react';

/**
 * Represents login page
 *
 * @param props Props containing reference to setUser function
 * @return {null}
 * @constructor
 */
export function Login(props) {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [error, setError] = useState("");
    const navigate = useNavigate();

    function submitForm(event) {
        event.preventDefault(); // Prevent default form submission
        console.log("Submitting form");
        sendAuthenticationRequest(username, password, onLoginSuccess, errorMessage => setError(errorMessage));
    }

    /**
     * This function is called when login is successful
     */
    function onLoginSuccess(userData) {
        props.setUser(userData);
        navigate("/");
    }

    let errorMessage = null;
    if (error) {
        errorMessage = <p className="error">{error}</p>;
    }

    return (
        <>
            <section id="create-account-section">
                <h2>Sign in</h2>
                <form id="sign-up-sheet">
                    <input className="input-field" type="text" name="username" placeholder="Username"
                           value={username} onChange={event => setUsername(event.target.value)}/>
                    <input className="input-field" type="password" name="password" placeholder="Password"
                           value={password} onChange={event => setPassword(event.target.value)}/>
                    {errorMessage}
                    <button type="submit" id="submit-button"
                            onClick={submitForm}>Sign in
                    </button>
                </form>
                <button id="change-page-button" onClick={() => navigate("/signup")}> Don't have an account?
                    Create one!
                </button>
            </section>
        </>
    )
}