import "../../css/Register.css";
import {useNavigate} from "react-router-dom";
import {useState} from "react";
import {sendApiRequest} from "../../tools/requests";
import React from 'react';

/**
 * Represents a signup page
 *
 * @returns {JSX.Element}
 * @constructor
 */
export function SignUp() {
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [error, setError] = useState("");
    const navigate = useNavigate();

    let errorMessage = null;
    if (error) {
        errorMessage = <p className="error">{error}</p>;
    }

    return (
        <div>
            <section id="create-account-section">
                <h2>Create Account</h2>
                <form id="sign-up-sheet">
                    <input className="input-field" type="text" id="username" name="username" placeholder="Username"
                           onChange={event => setUsername(event.target.value)}/>
                    <input className="input-field" type="password" id="pass" name="pass" placeholder="Password"
                           onChange={event => setPassword(event.target.value)}/>
                    <input className="input-field" type="password" id="con_pass" name="con_pass"
                           placeholder="Confirm password"
                           onChange={event => setConfirmPassword(event.target.value)}/>
                    {errorMessage}
                    <input id="submit-button" onClick={submitForm} type="submit" value="Sign up!"/>
                </form>
                <button id="change-page-button" onClick={() => navigate("/login")}>Have an account? Log in!</button>
            </section>
        </div>
    )

    /**
     * Submit the sign-up form
     * @param event
     */
    function submitForm(event) {
        event.preventDefault();
        if (password === confirmPassword) {
            const signupData = {
                "username": username,
                "password": password
            };
            sendApiRequest("POST", "/signup", onSignupSuccess, signupData, errorMessage => setError(errorMessage));
        } else {
            setError("Passwords do not match!");
        }

    }

    /**
     * This function is called when signup was successful
     */
    function onSignupSuccess() {
        navigate("/login");
    }

}