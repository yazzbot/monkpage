import "../css/ProfilePage.css";
import {deleteAuthorizationCookies} from "../tools/authentication";
import {useNavigate} from "react-router-dom";
import React from 'react';

/**
 * Represents the profile page.
 *
 * @param props
 * @returns {JSX.Element}
 * @constructor
 */
export default function ProfilePage(props) {
    const navigate = useNavigate();

    function signOut() {
        deleteAuthorizationCookies();
        onLoginSuccess();
    }

    /**
     * This function is called during signout
     */
    function onLoginSuccess() {

        console.log("Successfully signed out.")
        navigate("/");
        window.location.reload();
    }

    return <>
        <section id="user-profile">
            <h1 id="user-header">{props.user.username}</h1>
            <button id="your-orders-button" onClick={() => navigate("/order-history")}>
                Your orders
            </button>
            <button type="submit" id="signout-button"
                    onClick={signOut}>Sign out
            </button>
        </section>
    </>
}