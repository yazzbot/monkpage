import {getCookie} from "./cookies";
import axios from "axios";

const baseURL = "https://localhost:8443";

/**
 * Send a REST-API request to the backend
 *
 * @param method The method to use: GET, POST, PUT, DELETE
 * @param url relative URL of the API endpoint
 * @param callback Callback function to call on success, with response text as the parameter
 * @param requestBody When supplied, send this data in the request body. Does not work with HTTP GET!
 * @param errorCallback A function called when the response code is not 200
 */
export function sendApiRequest(method, url, callback, requestBody, errorCallback) {
    const request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (request.readyState === XMLHttpRequest.DONE) {
            if (request.status === 200) {
                let responseJson = "";
                if (request.responseText) {
                    responseJson = JSON.parse(request.responseText);
                }
                callback(responseJson);
            } else if (errorCallback) {
                errorCallback(request.responseText);
            } else {
                console.error("Error in API request");
            }
        }
    };
    request.open(method, baseURL + url);
    const jwtToken = getCookie("jwt");
    if (jwtToken) {
        request.setRequestHeader("Authorization", "Bearer " + jwtToken);
    }
    if (requestBody) {
        if (method.toLowerCase() !== "get") {
            request.setRequestHeader('Content-Type', 'application/json');
            request.send(JSON.stringify(requestBody));
        } else {
            console.error("Trying to send request data with HTTP GET, not allowed!")
            request.send();
        }
    } else {
        request.send();
    }
}

/**
 * Send a REST api put request
 *
 * @param url URL to send request to
 * @param product   Product data to be updated
 */
export function sendApiPutRequest(url, product) {
    axios.put(baseURL + url, product)
        .catch(function (error) {
            console.log(error)
        });
}

/**
 * Send a REST api delete request
 *
 * @param url   URL to send request to
 * @param callback Callback function to call on success, with response text as the parameter
 */
export function sendApiDeleteRequest(url, callback) {
    axios.delete(baseURL + url)
        .then(response => {
            callback = response.data;
        })
        .catch(function (error) {
            console.log(error)
        });
}