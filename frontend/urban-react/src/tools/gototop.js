import { useEffect } from "react";
import { useLocation } from "react-router-dom";

/**
 * Tool used to automatically go to top of page.
 *
 * @returns {null}
 * @constructor
 */
export default function Gototop() {
    const routePath = useLocation();
    const onTop = () => {
        window.scrollTo(0, 0);
    }
    useEffect(() => {
        onTop()
    }, [routePath]);

    return null;
}