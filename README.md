# appdev-webtek.gr11

# Intention
This project is based on the descriptions of project 3: Herbal teas. Following the requirements, we have developed an e-commerce website for company Urban Infusion, offering a variety of tea products. We have designed the website with ancient traditions in mind, and matched them together with modern trends within web design. 

# Setup instructions
The backend part of the project requires environment variables to run locally. We have implemented JWT for authentication, and MySQL as database. Hence, the .env file needs to contain the following variables:

`JWT_SECRET_KEY=` & `SPRING.DATASOURCE.PASSWORD=`,

where the latter's value needs to match the password of your local MySQL root user.
If a user other than 'root' is being used, this needs to be specified in `spring.datasource.username`, found in application.properties (/main/resources). 

With the .env file setup and enabled, the backend project should be ready to run from an IDE like IntelliJ.


The frontend part of the project can be run through commands `npm install` followed by `npm start` in terminal, from folder "\frontend\urban-react\", in an IDE like WebStorm.

