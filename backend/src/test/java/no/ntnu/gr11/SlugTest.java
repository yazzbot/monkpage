package no.ntnu.gr11;

import static org.junit.jupiter.api.Assertions.assertEquals;

import no.ntnu.gr11.tools.Slug;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

public class SlugTest {

    @Test
    @DisplayName("Test that slug generates correct string value")
    public void testCorrectGenerationOfSlug() {
        Slug slug = new Slug();

        assertEquals("herbal-tea", slug.makeSlug("Herbal Tea"));
        
    }
}
