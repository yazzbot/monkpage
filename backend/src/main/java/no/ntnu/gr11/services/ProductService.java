package no.ntnu.gr11.services;

import no.ntnu.gr11.models.Tag;
import no.ntnu.gr11.repositories.ProductRepository;
import no.ntnu.gr11.tools.Converter;
import no.ntnu.gr11.models.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Business logic related to products
 */
@Service
public class ProductService {
    @Autowired
    private ProductRepository productRepository;

    /**
     * Get all products currently stored in the application state
     *
     * @return All products
     */
    public List<Product> getAll() {
        return Converter.iterableToList(productRepository.findAll());
    }

    /**
     * Get a specific product in the application state
     *
     * @param id Id of product to find
     * @return Product with matching id, or null if none found
     */
    public Product findById(Integer id) {
        return productRepository.findById(id).orElse(null);
    }

    /**
     * Get a specific product in the application state by slug
     *
     * @param slug Slug of product to find
     * @return Product with matching slug
     */
    public Product findBySlug(String slug) {
        return productRepository.findBySlug(slug);
    }

    /**
     * Get a list of products in the application state containing title
     *
     * @param title Title of products to find
     * @return Product with matching slug
     */
    public List<Product> findByTitleContaining(String title) {
        return productRepository.findByTitleIgnoreCaseContaining(title);
    }

    /**
     * Add a product to the application state
     *
     * @param product Product to be added
     * @return True/false if product could be added
     */
    public boolean add(Product product) {
        boolean added = false;
        if (canBeAdded(product)) {
            product.setSlug(product.getTitle());
            productRepository.save(product);
            added = true;
        }
        return added;
    }

    /**
     * Check if product can be added to application state
     *
     * @param product Product to be checked
     * @return True/false if product is valid and can be added
     */
    private boolean canBeAdded(Product product) {
        return product != null && product.isValid()
                && (product.getID() == null || productRepository.findById(product.getID()).isEmpty());
    }

    /**
     * Delete a product from application state
     *
     * @param productId Id of product to be deleted
     * @return True/false if product was deleted
     */
    public boolean delete(int productId) {
        boolean deleted = false;
        if (findById(productId) != null) {
            productRepository.deleteById(productId);
            deleted = true;
        }
        return deleted;
    }

    /**
     * Update a product in the database
     *
     * @param id      The ID of the product to update
     * @param product the new data for the product
     * @return null on success, error message on error
     */
    public String update(int id, Product product) {
        Optional<Product> existingProduct = productRepository.findById(id);
        String errorMessage = null;
        if (existingProduct.isPresent()) {
            product.setId(id);
            try {
                product.setSlug(product.getTitle());
                productRepository.save(product);
            } catch (Exception e) {
                errorMessage = "Error while saving the product to DB: " + e.getMessage();
            }
        } else {
            errorMessage = "Product with given ID not found";
        }
        return errorMessage;
    }

    /**
     * Gets all products by tag assigned
     *
     * @param tags Tag of product to find
     * @return Products with matching tags
     */
    public List<Product> getByTag(Set<Tag> tags) {
        return productRepository.findByTagsIn(tags);
    }


}
