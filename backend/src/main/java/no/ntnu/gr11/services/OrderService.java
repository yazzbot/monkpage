package no.ntnu.gr11.services;

import no.ntnu.gr11.models.Order;
import no.ntnu.gr11.repositories.OrderRepository;
import no.ntnu.gr11.tools.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Business logic related to products
 */
@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    /**
     * Adds an order to the application state
     *
     * @param order Order to be added
     * @return True/false if order could be added
     */
    public boolean add(Order order) {
        boolean added = false;
        if (canBeAdded(order)) {
            orderRepository.save(order);
            added = true;
        }
        return added;
    }

    /**
     * Check if order can be added to application state
     *
     * @param order Order to be checked
     * @return True/false if order is valid and can be added
     */
    private boolean canBeAdded(Order order) {
        return order != null && order.isValid()
                && (order.getId() == null || orderRepository.findById(order.getId()).isEmpty());
    }

    /**
     * Get all order currently stored in the application
     *
     * @return All orders
     */
    public List<Order> getAll() {
        return Converter.iterableToList(orderRepository.findAll());
    }

    /**
     * Gets a specific order by ID
     *
     * @param id ID of order to find
     * @return Order with matching ID
     */
    public Order findById(Integer id) {
        return orderRepository.findById(id).orElse(null);
    }

    /**
     * Update an order in the database
     *
     * @param id    The ID of the order to find
     * @param order the new data for the order
     * @return null on success, error message on error
     */
    public String update(int id, Order order) {
        Order existingOrder = findById(id);
        String errorMessage = null;

        if (existingOrder == null) {
            errorMessage = "No order with id " + id + " found";
        }
        if (order == null || !order.isValid()) {
            errorMessage = "Wrong data in request body";
        } else if (order.getId() != id) {
            errorMessage = "Order ID in the URL does not match ID in JSON data response body";
        }
        if (errorMessage == null) {
            orderRepository.save(order);
        }
        return errorMessage;
    }

    /**
     * Delete an order from application state
     *
     * @param id ID of order to be deleted
     * @return True/false if order was deleted
     */
    public boolean delete(int id) {
        boolean deleted = false;
        if (findById(id) != null) {
            orderRepository.deleteById(id);
            deleted = true;
        }
        return deleted;
    }


}
