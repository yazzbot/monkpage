package no.ntnu.gr11.services;

import no.ntnu.gr11.models.Role;
import no.ntnu.gr11.repositories.RoleRepository;
import no.ntnu.gr11.repositories.UserRepository;
import no.ntnu.gr11.tools.Converter;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class RoleService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;


    /**
     * Get a specific role in the application state
     *
     * @param roleName Id of role to find
     * @return Role with matching name
     */
    public Role findByName(String roleName) {
        return roleRepository.findByName(roleName);
    }

    /**
     * Add a role to the application state
     *
     * @param role Role to be added
     * @return True/false if role could be added
     */
    public boolean add(Role role) {
        boolean added = false;
        if (canBeAdded(role)) {
            roleRepository.save(role);
            added = true;
        }
        return added;
    }

    /**
     * Check if role can be added to application state
     *
     * @param role Role to be checked
     * @return True/false if role is valid and can be added
     */
    private boolean canBeAdded(Role role) {
        return role != null && role.isValid()
                && (role.getId() == null || role.getName().isEmpty());
    }

    /**
     * Get all roles currently in application state
     *
     * @return All roles
     */
    public List<Role> getAllRoles() {
        return Converter.iterableToList(roleRepository.findAll());
    }
}
