package no.ntnu.gr11.services;

import no.ntnu.gr11.models.Category;
import no.ntnu.gr11.tools.Converter;
import no.ntnu.gr11.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Business logic related to Category
 */
@Service
public class CategoryService {
    @Autowired
    private CategoryRepository categoryRepository;


    /**
     * Gets all Categories
     *
     * @return all categories
     */
    public List<Category> getAll() {
        return Converter.iterableToList(categoryRepository.findAll());
    }

    /**
     * Get a specific category by ID
     *
     * @param id category ID
     * @return Category with matching id, or null if none found
     */
    public Category getById(Integer id) {
        return categoryRepository.findById(id).orElse(null);
    }

    /**
     * Get a specific category by title
     *
     * @param title Category title
     * @return Category with matching title
     */
    public Category getByTitle(String title) {
        return categoryRepository.findByTitle(title);
    }

    /**
     * Adds a Category to the application state
     *
     * @param category Category to be added
     * @return True/false if category could be added
     */
    public boolean add(Category category) {
        boolean added = false;
        if (canBeAdded(category)) {
            categoryRepository.save(category);
            added = true;
        }
        return added;
    }

    /**
     * Check if category can be added to application state
     *
     * @param category Category to be checked
     * @return True/false if category is valid and can be added
     */
    private boolean canBeAdded(Category category) {
        return category != null && category.isValid()
                && (category.getId() == null || categoryRepository.findById(category.getId()).isEmpty());
    }


    /**
     * Delete a category from application state
     *
     * @param categoryId Category to be checked
     * @return True/false if product is valid and can be added
     */
    public boolean delete(int categoryId) {
        boolean deleted = false;
        if (getById(categoryId) != null) {
            categoryRepository.deleteById(categoryId);
            deleted = true;
        }
        return deleted;
    }

    /**
     * Update a category in the database
     *
     * @param id       The ID of the category to update
     * @param category The new data for the category
     * @return null on success, error massage on error
     */
    public String update(int id, Category category) {
        Category existingCategory = getById(id);
        String errorMessage = null;

        if (existingCategory == null || !category.isValid()) {
            errorMessage = "No category with id " + id + " found.";
        }
        if (category == null || !category.isValid()) {
            errorMessage = "Wrong data in request body";
        } else if (category.getId() != id) {
            errorMessage = "Product ID in the URL does not match the ID in the JSON data response body";
        }
        if (errorMessage == null) {
            categoryRepository.save(category);
        }
        return errorMessage;
    }

    /**
     * Gets all categories by title
     *
     * @param title Title of the categories to find
     * @return Categories with matching title
     */
    public List<Category> getAllByTitle(String title) {
        return categoryRepository.findAllByTitle(title);
    }


}
