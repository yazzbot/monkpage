package no.ntnu.gr11.services;

import no.ntnu.gr11.models.Role;
import no.ntnu.gr11.models.User;
import no.ntnu.gr11.repositories.RoleRepository;
import no.ntnu.gr11.repositories.UserRepository;
import no.ntnu.gr11.tools.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;


    /**
     * Get a specific user in the application state
     *
     * @param username Id of user to find
     * @return User with matching id, or null if none found
     */
    public User findByUsername(String username) {
        return userRepository.findByUsername(username).orElse(null);
    }


    /**
     * Add a user to the application state
     *
     * @param user User to be added
     * @return True/false if user could be added
     */
    public boolean add(User user) {
        boolean added = false;
        if (canBeAdded(user)) {
            userRepository.save(user);
            added = true;
        }
        return added;
    }

    /**
     * Check if user can be added to application state
     *
     * @param user User to be checked
     * @return True/false if user is valid and can be added
     */
    private boolean canBeAdded(User user) {
        return user != null && user.isValid()
                && (user.getId() == null || user.getUsername().isEmpty());
    }

    public List<User> getAllUsers() {
        return Converter.iterableToList(userRepository.findAll());
    }

    public void registerDefaultUser(User user) {
        Role roleUser = roleRepository.findByName("USER");
        user.addRole(roleUser);

        userRepository.save(user);
    }
}