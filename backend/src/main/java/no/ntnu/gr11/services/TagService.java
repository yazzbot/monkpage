package no.ntnu.gr11.services;

import no.ntnu.gr11.models.Tag;
import no.ntnu.gr11.repositories.TagRepository;
import no.ntnu.gr11.tools.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Business logic related to product tags
 */
@Service
public class TagService {

    @Autowired
    private TagRepository tagRepository;

    /**
     * Get all tags currently stored in the application state
     *
     * @return All tags
     */
    public List<Tag> getAll() {
        return Converter.iterableToList(tagRepository.findAll());
    }

    /**
     * Get a specific tag in the application state
     *
     * @param id ID of tag to find
     * @return Tag with matching id, or null if none found
     */
    public Tag findById(Integer id) {
        return tagRepository.findById(id).orElse(null);
    }


    /**
     * Get a list of tags containing title
     *
     * @param title Title of tags to find
     * @return Tags with matching title
     */
    public List<Tag> findByTitleContaining(String title) {
        return tagRepository.findByTitleIgnoreCaseContaining(title);
    }

    /**
     * Adds a tag tot eh application state
     *
     * @param tag Tag to be added
     * @return True/false if tag could be added
     */
    public boolean add(Tag tag) {
        boolean added = false;
        if (canBeAdded(tag)) {
            tagRepository.save(tag);
            added = true;
        }
        return added;
    }

    /**
     * Check if tag can be added to application state
     *
     * @param tag Tag to be checked
     * @return True/false is valid and can be added
     */
    private boolean canBeAdded(Tag tag) {
        return tag != null && tag.isValid()
                && (tag.getTID() == null || tagRepository.findById(tag.getTID()).isEmpty());
    }

    /**
     * Delete a tag from application state
     *
     * @param tagId ID of tag to be deleted
     * @return True/false if tag was deleted
     */
    public boolean delete(int tagId) {
        boolean deleted = false;
        if (findById(tagId) != null) {
            tagRepository.deleteById(tagId);
            deleted = true;
        }
        return deleted;
    }

    /**
     * Update a tag in the database
     *
     * @param id  The ID of the tag to update
     * @param tag the new data for the product
     * @return null on success, error
     */
    public String update(int id, Tag tag) {
        Tag existingTag = findById(id);
        String errorMessage = null;

        if (existingTag == null) {
            errorMessage = "No product with id " + id + " found.";
        }
        if (tag == null || !tag.isValid()) {
            errorMessage = "Wrong data in request body.";
        } else if (tag.getTID() != id) {
            errorMessage = "Product ID in the URL does not match with the ID in JSON data response body.";
        }
        if (errorMessage == null) {
            tagRepository.save(tag);
        }
        return errorMessage;
    }

    /**
     * Get a list of tags with matching title
     *
     * @param title Title of tag to find
     * @return Tags with matching title
     */
    public List<Tag> getAllByTitle(String title) {
        return tagRepository.findByTitleIgnoreCaseContaining(title);
    }


}
