package no.ntnu.gr11.services;

import no.ntnu.gr11.models.ProductReview;
import no.ntnu.gr11.repositories.ProductReviewRepository;
import no.ntnu.gr11.tools.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Business logic related to product reviews
 */
@Service
public class ProductReviewService {
    @Autowired
    private ProductReviewRepository productReviewRepository;

    /**
     * Get all productReviews currently stored in the application state
     *
     * @return All productReviews
     */
    public List<ProductReview> getAll() {
        return Converter.iterableToList(productReviewRepository.findAll());
    }

    /**
     * Get a specific product review in the application state
     *
     * @param id Id of product review to find
     * @return Product review with matching id, or null if none found
     */
    public ProductReview findById(Integer id) {
        return productReviewRepository.findById(id).orElse(null);
    }

    /**
     * Add a product review to the application state
     *
     * @param productReview Product review to be added
     * @return True/false if product review could be added
     */
    public boolean add(ProductReview productReview) {
        boolean added = false;
        if (canBeAdded(productReview)) {
            productReviewRepository.save(productReview);
            added = true;
        }
        return added;
    }

    /**
     * Check if product review review can be added to application state
     *
     * @param productReview Product review to be checked
     * @return True/false if productReview is valid and can be added
     */
    private boolean canBeAdded(ProductReview productReview) {
        return productReview != null && productReview.isValid()
                && (productReview.getId() == null || productReviewRepository.findById(productReview.getId()).isEmpty());
    }

    /**
     * Delete a product review from application state
     *
     * @param productReviewId Id of product review to be deleted
     * @return True/false if product review was deleted
     */
    public boolean delete(int productReviewId) {
        boolean deleted = false;
        if (findById(productReviewId) != null) {
            productReviewRepository.deleteById(productReviewId);
            deleted = true;
        }
        return deleted;
    }

    /**
     * Update a product review in application state
     *
     * @param id            Id of product review to update
     * @param productReview The updated product review data
     * @return Null on success, error message on error
     */
    public String update(int id, ProductReview productReview) {
        ProductReview existingProductReview = findById(id);
        String errorMessage = null;

        if (existingProductReview == null) {
            errorMessage = "No productReview with id " + id + " found.";
        }
        if (productReview == null || !productReview.isValid()) {
            errorMessage = "Wrong data in request body.";
        } else if (productReview.getId() != id) {
            errorMessage = "ProductReview ID in the URL does not match the ID in JSON data response body.";
        }

        if (errorMessage == null) {
            productReviewRepository.save(productReview);
        }
        return errorMessage;
    }


    /**
     * Gets all reviews by rating
     *
     * @param rating rating of reviews to find
     * @return Reviews with matching rating
     */
    public List<ProductReview> getAllByRating(int rating) {
        return productReviewRepository.findByRating(rating);
    }


}
