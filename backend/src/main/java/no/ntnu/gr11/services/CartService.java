package no.ntnu.gr11.services;

import no.ntnu.gr11.models.Cart;
import no.ntnu.gr11.repositories.CartRepository;
import no.ntnu.gr11.tools.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Business logic related to cart
 */
@Service
public class CartService {
    @Autowired
    private CartRepository cartRepository;


    /**
     * Gets all carts currently in stored in the application
     * @return
     */
    public List<Cart> getAll(){
        return Converter.iterableToList(cartRepository.findAll());
    }


    /**
     * Adds a cart to the application state
     * @param cart Cart to be added
     * @return true/false if cart added
     */
    public boolean add(Cart cart){
        boolean added = false;
        if(canBeAdded(cart)){
            cartRepository.save(cart);
            added = true;
        }
        return added;
    }


    /**
     * Checks if a cart can be added to application state
     * @param cart cart to be checked
     * @return True/false if cart is valid and can be added
     */
    private boolean canBeAdded(Cart cart){
        return cart !=null && (cart.getId() == null || cartRepository.findById(cart.getId()).isEmpty());
    }

    /**
     * Get a specific cart by ID
     * @param id ID of cart to find
     * @return Cart with matching ID, or null if none found
     */
    public Cart findById(Integer id){
        return cartRepository.findById(id).orElse(null);
    }


    /**
     * Update a cart in the database
     * @param id the ID of the cart to update
     * @param cart The new data for the cart
     * @return null on success, error message on error
     */
    public String update(int id, Cart cart){
        Cart existingCart = findById(id);
        String errorMessage = null;

        if(existingCart == null || !cart.isValid()){
            errorMessage = "No category with id "+ id + " found.";
        }
        if(cart == null || !cart.isValid()){
            errorMessage = "Wrong data in request body";
        }
        else if(cart.getId() != id){
            errorMessage= "Product ID in the URL does not match the ID in the JSON data response body";
        }
        if(errorMessage == null){
            cartRepository.save(cart);
        }
        return errorMessage;
    }

    /**
     * Deletes a cart from application state
     * @param cartId
     * @return
     */
    public boolean delete(int cartId){
        boolean deleted = false;
        if(findById(cartId) != null){
            cartRepository.deleteById(cartId);
            deleted = true;
        }
        return deleted;
    }
}
