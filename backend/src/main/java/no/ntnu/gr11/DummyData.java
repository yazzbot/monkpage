package no.ntnu.gr11;

import no.ntnu.gr11.models.*;
import no.ntnu.gr11.repositories.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.*;


@Component
public class DummyData implements ApplicationListener<ApplicationReadyEvent> {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private TagRepository tagRepository;

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderItemRepository orderItemRepository;

    private final Logger logger = LoggerFactory.getLogger("DummyInit");

    /**
     * This method is called when the application is ready (loaded)
     *
     * @param event Event which we don't use :)
     */
    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        Optional<User> existingAdminUser = userRepository.findByUsername("admin");
        if (existingAdminUser.isEmpty()) {
            logger.info("Importing default user data...");
            User admin = new User("admin", "$2a$12$FBQPle1xPUVJgKOc/EiHMOx4hAq2xKwko0B2ojHZvBaKiUWRkV24W");
            User user = new User("user", "$2a$12$x.ZjNH3PyYf9AXYP/dNcBuzLNb4OrRUukF99xxri5IZcsG3.foHly");
            //Creates new users
            Role userRole = new Role("ROLE_USER");
            Role adminRole = new Role("ROLE_ADMIN");
            //Creates new roles
            admin.addRole(userRole);
            admin.addRole(adminRole);
            user.addRole(userRole);
            //Assigns roles to the users

            roleRepository.save(userRole);//saves user role to the database
            roleRepository.save(adminRole);//saves admin role to the database

            userRepository.save(admin);
            userRepository.save(user);

            logger.info("Importing default product data...");

            // Create products
            Product heatherTea = new Product("Heather tea",
                    "Gathered carefully before the bees to hold the honey taste. " +
                            "Rich in vitamins, locally produced.", 3, 200, 1,
                    "https://i.etsystatic.com/23530158/r/il/8dad27/2407360226/il_570xN.2407360226_e5w0.jpg");
            Product lindenBlossomTea = new Product("Linden blossom tea",
                    "Classic Latvian tea. Helps against laziness. Gathered in summer 2021. " +
                    "PS! Use 100 degree C water.", 14, 200, 1,
                    "https://c-fa.niceshops.com/upload/image/product/large/default/my-herbs-linden-blossom-tea-30-g-249617-en.jpg");
            Product sencha = new Product("Sencha", "Made from green leaves. " +
                    "Available in August-September season only.", 1, 100, 1,
                    "https://cdn.shopify.com/s/files/1/0683/7827/products/norges-tehus_sencha-first-flush_c.jpg?v=1619202272");
            Product classicMug = new Product("Classic mug",
                    "Classic mug made from Brazilian clay with handy handle! " +
                    "Hot-friendly - comfortable to hold even when the water is hot", 4, 120, 1,
                    "https://nordecor-i02.mycdn.no/mysimgprod/nordecor_mystore_no/images/23959_ZAKKIA_Kopp_-_Gr__-_Classic_Mug_Zakkia_1.jpg/w600h600.jpg");
            Product whiteTea = new Product("White Tea",
                    "The best white tea you'll ever find", 3, 139, 1,
                    "https://www.thehopeandglory.co.uk/wp-content/uploads/2019/09/white.jpg");
            Product englishBreakfast = new Product("English Breakfast",
                    "The best herbal tea you'll ever find", 12, 199, 1,
                    "https://www.justorganictea.com/wp-content/uploads/2021/02/english-breakfast-tea.jpg");
            Product appleTea = new Product("Apple Tea",
                    "The best apple tea you'll ever find", 9, 219, 1,
                    "https://www.davidstea.com/dw/image/v2/BBXZ_PRD/on/demandware.static/-/Sites-davidstea-master-catalog/default/dwb1eb5e0d/productimages/10929DT01VAR0092710-10929US01VAR0070761-BI-1.jpg?sw=450&sh=450&sm=fit");
            Product morningTea = new Product("Morning Tea",
                    "The best morning tea you'll ever find", 10, 179, 1,
                    "https://www.iconbeauty.no/images/product/camilla%20pihl/home/CamillaPihl_GoodMorning-Teabag800x800-p.jpg");


            Category blackTeaCategory = new Category("Black-Tea","All teas with black tea");

            //Creates new Tags
            Tag tag = new Tag("Green-Tea");
            Tag tag2 = new Tag("Black-Tea");
            Tag tag3 = new Tag("Herbal-Tea");
            Tag tag4 = new Tag("Fruit-Tea");
            Tag tag5 = new Tag("Tea-Mug");

            //Creates a cart to add products
            Cart cart = new Cart("Cart1");


            heatherTea.setTags(tag4);
            heatherTea.setTags(tag2);
            lindenBlossomTea.setTags(tag2);
            lindenBlossomTea.setTags(tag4);
            sencha.setTags(tag);
            classicMug.setTags(tag5);
            whiteTea.setTags(tag2);
            englishBreakfast.setTags(tag2);
            appleTea.setTags(tag4);
            morningTea.setTags(tag2);

            classicMug.setCategory(blackTeaCategory);
            whiteTea.setCategory(blackTeaCategory);


            lindenBlossomTea.addToCart(cart);
            sencha.addToCart(cart);


            tagRepository.save(tag);
            tagRepository.save(tag2);
            tagRepository.save(tag3);
            tagRepository.save(tag4);
            tagRepository.save(tag5);


            cartRepository.save(cart);
            categoryRepository.save(blackTeaCategory);


            productRepository.save(heatherTea);
            productRepository.save(lindenBlossomTea);
            productRepository.save(sencha);
            productRepository.save(classicMug);
            productRepository.save(whiteTea);
            productRepository.save(englishBreakfast);
            productRepository.save(appleTea);
            productRepository.save(morningTea);

            Order order1 = new Order(1000, admin);
            Order order2 = new Order(816, admin);

            OrderItem orderItem1 = new OrderItem(order1, heatherTea, 2, 1250);
            OrderItem orderItem2 = new OrderItem(order1, lindenBlossomTea, 3, 1750);
            OrderItem orderItem3 = new OrderItem(order2, sencha, 1, 200);
            OrderItem orderItem4 = new OrderItem(order2, morningTea, 4, 800);

            orderRepository.save(order1);
            orderRepository.save(order2);
            orderItemRepository.save(orderItem1);
            orderItemRepository.save(orderItem2);
            orderItemRepository.save(orderItem3);
            orderItemRepository.save(orderItem4);








            logger.info("DONE importing static data");
        } else {
            logger.info("Static data already in the database, not importing anything");
        }
    }
}