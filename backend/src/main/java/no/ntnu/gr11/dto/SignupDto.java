package no.ntnu.gr11.dto;

/**
 * Data transfer object (DTO) for data from the sign-up form
 *
 * @author Group 11
 */
public class SignupDto {
    private final String username;
    private final String password;

    public SignupDto(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
