package no.ntnu.gr11.dto;

/**
 * Data that will be sent as a response to the user when the authentication is successful
 *
 * @author Group 11
 */
public class AuthenticationResponse {
    private final String jwt;

    public AuthenticationResponse(String jwt) {
        this.jwt = jwt;
    }

    public String getJwt() {
        return jwt;
    }
}