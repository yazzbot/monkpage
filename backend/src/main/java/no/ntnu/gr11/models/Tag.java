package no.ntnu.gr11.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import java.util.*;

/**
 * Represents a tag entity, stored in the application state.
 *
 * @author Group 11
 */
@Entity
@Table(name = "tag")
public class Tag {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer tID;
    @Column(name = "title")
    private String title;

    @ManyToMany(mappedBy = "tags", cascade = CascadeType.MERGE)
    @JsonIgnore
    private Set<Product> products = new LinkedHashSet<>();

    public Tag(String title) {
        this.title = title;
    }

    public Tag() {

    }

    public Integer getTID() {
        return tID;
    }

    public void setTID(Integer id) {
        this.tID = id;
    }

    public boolean isValid() {
        return (this.tID == null || tID < 0) && !"".equals(title);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }
}
