package no.ntnu.gr11.models;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Represents a product review entity, stored in the application state (database).
 */
@Entity
@Table(name = "product_review")
public class ProductReview {
    @Id
    @GeneratedValue
    private Integer id;
    @Column(name = "title")
    private String title;
    @Column(name = "rating")
    private Integer rating;
    @Column(name = "published")
    private boolean published;
    @Column(name = "created_at")
    private LocalDateTime createdAt;
    @Column(name = "published_at")
    private LocalDateTime publishedAt;
    @Column(name = "content")
    private String content;


    public ProductReview(String title, String content) {
        this.title = title;
        this.content = content;
        this.createdAt = LocalDateTime.now();
        this.publishedAt = LocalDateTime.now();
    }

    public ProductReview() {

    }


    /**
     * Checks if a product is valid (if id is null and larger than 0, or title is empty)
     *
     * @return True/false if product is valid
     */
    public boolean isValid() {
        return (this.id == null || id > 0) && !"".equals(title);
    }


    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public LocalDateTime getPublishedAt() {
        return publishedAt;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
