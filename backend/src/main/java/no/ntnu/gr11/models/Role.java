package no.ntnu.gr11.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.*;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Represents a role, stored in the application state.
 *
 * @author Group 11
 */
@Entity
@Table(name = "roles")
public class Role {
    @Id
    @GeneratedValue
    private Long rID;
    @Column(name = "role_name")
    private String name;

    @ManyToMany(cascade = CascadeType.MERGE)
    @JsonIgnore
    private Set<User> users = new LinkedHashSet<>();

    public Role() {
    }

    public Role(String name) {
        this.name = name;

    }

    /**
     * Checks if a role is valid (if id is null and larger than 0, or name is empty)
     *
     * @return True/false if product is valid
     */
    public boolean isValid() {
        return (this.rID == null || rID > 0)
                && !("".equals(this.name) || this.name == null);
    }

    public Long getId() {
        return rID;
    }

    public void setId(Long id) {
        this.rID = id;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
