package no.ntnu.gr11.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import no.ntnu.gr11.tools.Slug;
import javax.persistence.*;
import java.time.LocalDate;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Represents a product, stored in the application state (database).
 */
@Entity
@Table(name = "product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "title")
    private String title;
    @Column(name = "slug")
    private String slug;
    @Column(name = "summary")
    private String summary;
    @Column(name= "sku")
    private Integer sku;
    @Column(name = "price")
    private Integer price;
    @Column(name = "discount")
    private float discount;
    @Column(name= "quantity")
    private Integer quantity;
    @Column(name = "created_at")
    private LocalDate createdAt;
    @Column(name = "updated_at")
    private LocalDate updatedAt;

    @Column(name = "image_url")
    private String imageURL;



    @JsonIgnore
    @ManyToOne
    private Category category;


    @JsonIgnore
    @ManyToOne
    private Cart cart;


    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinTable(name = "product_tag",
            joinColumns = @JoinColumn(name="pid"),
            inverseJoinColumns = @JoinColumn(name="tid")
    )
    private Set<Tag> tags = new LinkedHashSet<>();



    public Product(String title, String summary, int sku, int price, int quantity, String imageURL) {
        this.title = title;
        this.summary = summary;
        this.sku = sku;
        this.price = price;
        this.discount = 0;
        this.quantity = quantity;
        this.createdAt = LocalDate.now();
        this.updatedAt = LocalDate.now();
        this.imageURL = imageURL;
        setSlug(title);
    }

    public Product(int id, String title, String summary, int sku, int price, float discount, int quantity, String imageURL) {
        this.id = id;
        this.title = title;
        this.summary = summary;
        this.sku = sku;
        this.price = price;
        this.discount = discount;
        this.quantity = quantity;
        this.createdAt = LocalDate.now();
        this.updatedAt = LocalDate.now();
        this.imageURL = imageURL;
        setSlug(title);

    }

    public Product() {

    }



    /**
     * Sets a slug based on product's title. Called upon at creation of new Product instance.
     *
     * @param title   Title of product to be converted into slug
     */
    public void setSlug(String title) {
        this.slug = Slug.makeSlug(title);
    }

    /**
     * Checks if a product is valid (if id is null and larger than 0, or title is empty)
     *
     * @return  True/false if product is valid
     */
    public boolean isValid() {
        return (this.id == null || id > 0) && !"".equals(title);
    }


    public Integer getID() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public String getTitle() {
        return title;
    }


    public void setTitle(String title) {
        this.title = title;
    }


    public String getSlug() {
        return slug;
    }


    public String getSummary() {
        return summary;
    }


    public void setSummary(String summary) {
        this.summary = summary;
    }


    public int getSku() {
        return sku;
    }


    public void setSku(int sku) {
        this.sku = sku;
    }


    public float getPrice() {
        return price;
    }


    public void setPrice(int price) {
        this.price = price;
    }


    public float getDiscount() {
        return discount;
    }


    public void setDiscount(float discount) {
        this.discount = discount;
    }


    public int getQuantity() {
        return this.quantity;
    }


    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }


    public LocalDate getCreatedAt() {
        return createdAt;
    }


    public LocalDate getUpdatedAt() {
        return updatedAt;
    }


    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Tag tag) {
        tags.add(tag);
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public boolean hasTag(String tagTitle) {
        boolean found = false;
        Iterator<Tag> it = tags.iterator();
        while (!found && it.hasNext()) {
            Tag tag = it.next();
            if (tag.getTitle().equalsIgnoreCase(tagTitle)) {
                found = true;
            }
        }
        return found;
    }


    public Cart getCart() {
        return cart;
    }

    public void addToCart(Cart cart){
        this.cart = cart;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }


}
