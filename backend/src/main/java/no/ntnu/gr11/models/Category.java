package no.ntnu.gr11.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

/**
 * Represents a category entity, stored in the application state.
 *
 * @author Group 11
 */
@Entity
@Table(name = "category")
public class Category {

    @Id
    @GeneratedValue
    private Integer id;
    @Column(name = "title")
    private String title;
    @Column(name = "meta_title")
    private String metaTitle;
    @Column(name = "slug")
    private String slug;
    @Column(name = "content")
    private String content;



    @OneToMany(mappedBy="category")
    private List<Product> categoryProducts = new LinkedList<>();


    public Category(String title, String content) {
        this.title = title;
        this.content = content;
        setSlug(title);
    }

    public Category() {

    }

    public String getTitle() {
        return title;
    }

    public Integer getId() {
        return id;
    }

    public String getMetaTitle() {
        return metaTitle;
    }

    public String getContent() {
        return content;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setMetaTitle(String metaTitle) {
        this.metaTitle = metaTitle;
    }

    public void setContent(String content) {
        this.content = content;
    }


    public void setSlug(String slug) {
        this.slug = slug;
    }

    public List<Product> getProducts() {
        return categoryProducts;
    }

    public void setProducts(List<Product> products) {
        this.categoryProducts = products;
    }

    /**
     * Returns true if category fields are valid
     *
     * @return True/false if category fields are valid
     */
    public boolean isValid() {
        return (this.id == null || id > 0) && !"".equals(title);
    }
}
