package no.ntnu.gr11.controllers;

import no.ntnu.gr11.dto.UserProfileDto;
import no.ntnu.gr11.models.User;
import no.ntnu.gr11.services.AccessUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * REST API controller responsible for users
 *
 * @author Group 11
 */
@CrossOrigin
@RestController
public class UserController {
    @Autowired
    private AccessUserService userService;

    /**
     * Return user profile information
     *
     * @param username Username for which the profile is requested
     * @return The profile information or error code when not authorized
     */
    @GetMapping("/users/{username}")
    public ResponseEntity<?> getProfile(@PathVariable String username) throws InterruptedException {
        User sessionUser = userService.getSessionUser();
        // Check for authenticated user and matching usernames
        if (sessionUser != null && sessionUser.getUsername().equals(username)) {
            UserProfileDto profile = new UserProfileDto(sessionUser.getBio());
            Thread.sleep(2000); // Simulate sleep
            return new ResponseEntity<>(profile, HttpStatus.OK);
        } else if (sessionUser == null) {
            return new ResponseEntity<>("Profile data accessible only to authenticated users", HttpStatus.UNAUTHORIZED);
        } else {
            return new ResponseEntity<>("Profile data for other users not accessible!", HttpStatus.FORBIDDEN);
        }
    }

    /**
     * Update user profile information
     *
     * @param username Username for which the profile is updated
     * @return HTTP 200 OK or error code with error message
     */
    @PutMapping("/users/{username}")
    public ResponseEntity<String> updateProfile(@PathVariable String username, @RequestBody UserProfileDto profileData) throws InterruptedException {
        User sessionUser = userService.getSessionUser();
        ResponseEntity<String> response;
        // Check for authenticated user and matching usernames
        if (sessionUser != null && sessionUser.getUsername().equals(username)) {
            if (profileData != null) {
                if (userService.updateProfile(sessionUser, profileData)) {
                    Thread.sleep(2000); // Simulate long operation
                    response = new ResponseEntity<>("", HttpStatus.OK);
                } else {
                    response = new ResponseEntity<>("Could not update profile data", HttpStatus.INTERNAL_SERVER_ERROR);
                }
            } else {
                response = new ResponseEntity<>("Profile data not supplied", HttpStatus.BAD_REQUEST);
            }
        } else if (sessionUser == null) {
            response = new ResponseEntity<>("Profile data accessible only to authenticated users", HttpStatus.UNAUTHORIZED);
        } else {
            response = new ResponseEntity<>("Profile data for other users not accessible!", HttpStatus.FORBIDDEN);
        }
        return response;
    }
    /**
     * Delete a user
     *
     * @param username username of user to be deleted
     * @return HTTP status 200 OK on success, 404 Not found on error
     */
    @DeleteMapping("/users/{username}")
    public ResponseEntity<String> delete(@PathVariable String username) {
        ResponseEntity<String> response;
        if (this.userService.deleteUser(username)) {
            response = new ResponseEntity<>(HttpStatus.OK);
        } else {
            response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return response;
    }
}
