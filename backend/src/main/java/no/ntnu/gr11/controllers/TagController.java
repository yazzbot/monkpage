package no.ntnu.gr11.controllers;

import no.ntnu.gr11.models.Product;
import no.ntnu.gr11.models.Tag;
import no.ntnu.gr11.repositories.ProductRepository;
import no.ntnu.gr11.repositories.TagRepository;
import no.ntnu.gr11.services.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * REST controller responsible for tags
 *
 * @author Group 11
 */
@CrossOrigin
@RestController
@RequestMapping("tag")
public class TagController {

    @Autowired
    private TagService tagService;

    /**
     * Return all tags stored in app state
     *
     * @param title Optional: Title filtering
     * @return All tags stored in app state; All tags by filter
     */
    @GetMapping
    public List<Tag> getAll(@RequestParam(required = false) String title) {
        if (title != null && !"".equals(title)) {
            return tagService.getAllByTitle(title);
        } else {
            return tagService.getAll();
        }
    }

    /**
     * Return a specific tag stored in the app state by name
     *
     * @param tag Tag name to find by
     * @return Tags with matching name
     */
    @GetMapping("/{tag}")
    public List<Tag> getByTagContaining(@PathVariable String tag) {
        return tagService.findByTitleContaining(tag);
    }

    /**
     * Add a tag to the app state
     *
     * @param tag Tag data to be added
     * @return OK on success; BAD REQUEST on failure
     */
    @PostMapping
    public ResponseEntity<String> add(@RequestBody Tag tag) {
        ResponseEntity<String> response;
        if (this.tagService.add(tag)) {
            response = new ResponseEntity<>(HttpStatus.OK);
        } else {
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * Delete a tag from the app state
     *
     * @param id id of tag to be deleted
     * @return OK on success; NOT FOUND on failure
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable int id) {
        ResponseEntity<String> response;
        if (this.tagService.delete(id)) {
            response = new ResponseEntity<>(HttpStatus.OK);
        } else {
            response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return response;
    }

    /**
     * Update a tag on the app state
     *
     * @param id  id of tag to be updated
     * @param tag New tag data to be stored
     * @return OK on success; BAD REQUEST + error message on failure
     */
    @PutMapping("/{id}")
    public ResponseEntity<String> update(@PathVariable int id, @RequestBody Tag tag) {
        String errorMessage = tagService.update(id, tag);
        ResponseEntity<String> response;
        if (errorMessage == null) {
            response = new ResponseEntity<>(HttpStatus.OK);
        } else {
            response = new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * Return tags by title containing a keyword
     *
     * @param title Keyword to find by
     * @return Tags containing keyword
     */
    @GetMapping("/{title}")
    public List<Tag> getByTitleContaining(@PathVariable String title) {
        return tagService.findByTitleContaining(title);
    }
}

