package no.ntnu.gr11.controllers;

import no.ntnu.gr11.models.Cart;
import no.ntnu.gr11.repositories.CartRepository;
import no.ntnu.gr11.services.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;

/**
 * REST controller responsible for cart
 *
 * @author Group 11
 */
@CrossOrigin
@RestController
@RequestMapping("cart")
public class CartController {

    @Autowired
    CartRepository cartRepository;
    @Autowired
    CartService cartService;

    /**
     * Return all cart items in app state
     *
     * @return All cart items
     */
    @GetMapping("")
    public List<Cart> getAll() {
        return cartService.getAll();
    }

    /**
     * Return a specific cart from app state by id
     *
     * @param id Cart id
     * @return OK + cart item; Or NOT FOUND
     */
    @GetMapping("/{id}")
    public ResponseEntity<Cart> getOne(@PathVariable Integer id) {
        ResponseEntity<Cart> response;
        Cart cart = this.cartService.findById(id);
        if (cart != null) {
            response = new ResponseEntity<>(cart, HttpStatus.OK);
        } else {
            response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return response;
    }

    /**
     * Add a cart item to app state
     *
     * @param cart Cart item to be added
     * @return OK on success; BAD REQUEST on failure
     */
    @PostMapping
    public ResponseEntity<String> add(@RequestBody Cart cart) {
        ResponseEntity<String> response;
        if (this.cartService.add(cart)) {
            response = new ResponseEntity<>(HttpStatus.OK);
        } else {
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * Update a cart item on the app state
     *
     * @param id   Id of cart to be updated
     * @param cart New cart data to be saved
     * @return OK on success; BAD REQUEST + error message on failure
     */
    @PutMapping("/{id}")
    public ResponseEntity<String> updateCart(@PathVariable("id") Integer id, @RequestBody Cart cart) {
        ResponseEntity<String> response;
        String errorMessage = cartService.update(id, cart);
        if (errorMessage == null) {
            response = new ResponseEntity<>(HttpStatus.OK);
        } else {
            response = new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * Delete a cart from app state
     *
     * @param id Id of cart item to be deleted
     * @return OK on success; NOT FOUND on failure
     */
    @DeleteMapping
    public ResponseEntity<String> delete(@PathVariable int id) {
        ResponseEntity<String> response;
        if (this.cartService.delete(id)) {
            response = new ResponseEntity<>(HttpStatus.OK);
        } else {
            response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return response;
    }
}
