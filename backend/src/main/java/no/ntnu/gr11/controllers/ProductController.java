package no.ntnu.gr11.controllers;

import no.ntnu.gr11.models.Cart;
import no.ntnu.gr11.models.Tag;
import no.ntnu.gr11.repositories.CartRepository;
import no.ntnu.gr11.repositories.ProductRepository;
import no.ntnu.gr11.services.CartService;
import no.ntnu.gr11.services.ProductService;
import no.ntnu.gr11.models.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

/**
 * REST controller for products
 */
@CrossOrigin
@RestController
@RequestMapping("products")
public class ProductController {
    @Autowired
    private ProductService productService;


    /**
     * Returns all products
     * Sends HTTP GET to /products
     *
     * @param tagTitle Product title. When specified, get all books containing this substring
     * @return List of all products
     */
    @GetMapping
    public List<Product> getAll(@RequestParam(required = false, name = "category") String tagTitle) {
        // Store all products on app state, used to handle filtering
        List<Product> productsList = productService.getAll();

        if (tagTitle != null && !"".equals(tagTitle)) {
            productsList = filterProductsByTagTitle(productsList, tagTitle);
        }
        return productsList;
    }

    /**
     * Filter products in list by tag title
     *
     * @param collection List to be filtered
     * @param title      Title to filter by
     * @return Filtered list
     */
    public List<Product> filterProductsByTagTitle(List<Product> collection, String title) {
        return collection
                .stream()
                .filter(b -> b.hasTag(title))
                .collect(Collectors.toList());
    }

    /**
     * Return products by tag
     *
     * @param tag Tag to match products
     * @return Products with matching tag
     */
    @GetMapping("/tag/{tag}")
    public List<Product> getByTag(@PathVariable Set<Tag> tag) {
        return productService.getByTag(tag);
    }

    /**
     * Returns the 3 first products in the application state.
     * Used for best sellers section.
     *
     * @return 3 first products in app state
     */
    @GetMapping("/best-sellers")
    public List<Product> getFirstThree() {
        Iterator<Product> it = this.productService.getAll().iterator();
        ArrayList<Product> products = new ArrayList<>();
        while (products.size() < 3) {
            products.add(it.next());
        }
        return products;
    }

    /**
     * Returns all products with a title containing parameter "title"
     *
     * @param title String used to search
     * @return Product(s) containing string parameter
     */
    @GetMapping("/search/{title}")
    public List<Product> getByTitleContaining(@PathVariable String title) {
        return productService.findByTitleContaining(title);
    }

    /**
     * Get a specific product
     *
     * @param id Product id
     * @return Product with matching id or HTTP status 404
     */
    @GetMapping("/{id}")
    public ResponseEntity<Product> getOne(@PathVariable Integer id) {
        ResponseEntity<Product> response;
        Product product = this.productService.findById(id);
        if (product != null) {
            response = new ResponseEntity<>(product, HttpStatus.OK);
        } else {
            response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return response;
    }

    /**
     * Returns single product with matching slug.
     *
     * @param slug Slug to match
     * @return Product with matching slug
     */
    @GetMapping("/tea/{slug}")
    public ResponseEntity<Product> getBySlug(@PathVariable String slug) {
        ResponseEntity<Product> response;
        Product product = this.productService.findBySlug(slug);
        if (product != null) {
            response = new ResponseEntity<>(product, HttpStatus.OK);
        } else {
            response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return response;
    }

    /**
     * Add a product
     *
     * @param product Product to be added, from HTTP response body
     * @return HTTP status 200 OK on success, 400 Bad request on error
     */
    @PostMapping
    public ResponseEntity<String> add(@RequestBody Product product) {
        ResponseEntity<String> response;
        if (this.productService.add(product)) {
            response = new ResponseEntity<>(HttpStatus.OK);
        } else {
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * Delete a product
     *
     * @param id Id of product to be deleted
     * @return HTTP status 200 OK on success, 404 Not found on error
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable int id) {
        ResponseEntity<String> response;
        if (this.productService.delete(id)) {
            response = new ResponseEntity<>(HttpStatus.OK);
        } else {
            response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return response;
    }

    /**
     * Update a product
     *
     * @param id      Id of product to be updated, from URL
     * @param product New product data to be stored, from request body
     * @return HTTP status 200 OK on success, 400 Bad request on error
     */
    @PutMapping("/{id}")
    public ResponseEntity<String> update(@PathVariable int id, @RequestBody Product product) {
        ResponseEntity<String> response;
        String errorMessage = productService.update(id, product);
        if (errorMessage == null) {
            response = new ResponseEntity<>(HttpStatus.OK);
        } else {
            response = new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }
        return response;
    }
}
