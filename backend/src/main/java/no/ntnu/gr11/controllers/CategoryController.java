package no.ntnu.gr11.controllers;


import no.ntnu.gr11.models.Category;
import no.ntnu.gr11.services.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * REST controller responsible for categories
 *
 * @author Group 11
 */
@CrossOrigin
@RestController
@RequestMapping("category")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    /**
     * Return all categories stored in app state
     *
     * @return All categories in app state
     */
    @GetMapping
    public List<Category> getAll() {
        return categoryService.getAll();

    }

    /**
     * Return a specific category from the app state based on category title
     *
     * @param category Title of category to return
     * @return Specific category based on title
     */
    @GetMapping("/{category}")
    public ResponseEntity<Category> getCategory(@PathVariable String category) {
        ResponseEntity<Category> response;
        Category selectedCategory = this.categoryService.getByTitle(category);
        if (category != null) {
            response = new ResponseEntity<>(selectedCategory, HttpStatus.OK);
        } else {
            response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return response;
    }

    /**
     * Add a category to the app state
     *
     * @param category Body of category to be added
     * @return OK on success; BAD REQUEST on failure
     */
    @PostMapping
    public ResponseEntity<String> add(@RequestBody Category category) {
        ResponseEntity<String> response;
        if (this.categoryService.add(category)) {
            response = new ResponseEntity<>(HttpStatus.OK);
        } else {
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * Delete a category from the app state
     *
     * @param id id of category to be deleted
     * @return OK on success; NOT FOUND if category is not present
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable int id) {
        ResponseEntity<String> response;
        if (this.categoryService.delete(id)) {
            response = new ResponseEntity<>(HttpStatus.OK);
        } else {
            response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return response;
    }

    /**
     * Update a category on the app state
     *
     * @param id       id of category to be updated
     * @param category New category data to be stored
     * @return OK on success; BAD REQUEST on failure
     */
    @PutMapping("/{id}")
    public ResponseEntity<String> update(@PathVariable int id, @RequestBody Category category) {
        String errorMessage = categoryService.update(id, category);
        ResponseEntity<String> response;
        if (errorMessage == null) {
            response = new ResponseEntity<>(HttpStatus.OK);
        } else {
            response = new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }
        return response;
    }
}
