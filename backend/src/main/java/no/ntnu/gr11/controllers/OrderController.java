package no.ntnu.gr11.controllers;

import no.ntnu.gr11.models.Order;
import no.ntnu.gr11.services.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * REST controller responsible for orders
 *
 * @author Group 11
 */
@CrossOrigin
@RestController
@RequestMapping("orders")
public class OrderController {
    @Autowired
    private OrderService orderService;

    /**
     * Return all orders stored in app state
     *
     * @param title Optional: Title filter param
     * @return All orders stored in app state; All orders filtered
     */
    @GetMapping
    public List<Order> getAll(@RequestParam(required = false) String title) {
        return orderService.getAll();
    }

    /**
     * Return a specific order stored in app state by id
     *
     * @param id id of order to be returned
     * @return Order item + OK on success; NOT FOUND on failure
     */
    @GetMapping("/{id}")
    public ResponseEntity<Order> getOne(@PathVariable Integer id) {
        ResponseEntity<Order> response;
        Order order = this.orderService.findById(id);
        if (order != null) {
            response = new ResponseEntity<>(order, HttpStatus.OK);
        } else {
            response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return response;
    }

    /**
     * Add an order to the app state
     *
     * @param order Order data to be added
     * @return OK on success; BAD REQUEST on failure
     */
    @PostMapping("")
    public ResponseEntity<String> add(@RequestBody Order order) {
        ResponseEntity<String> response;
        if (this.orderService.add(order)) {
            response = new ResponseEntity<>(HttpStatus.OK);
        } else {
            response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return response;
    }

    /**
     * Delete an order from app state
     *
     * @param id id of order to be deleted
     * @return OK on success; NOT FOUND on failure
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable int id) {
        ResponseEntity<String> response;
        if (this.orderService.delete(id)) {
            response = new ResponseEntity<>(HttpStatus.OK);
        } else {
            response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return response;
    }

    /**
     * Update an order on the app state
     *
     * @param id    id of order to be updated
     * @param order New order data to be stored
     * @return OK on success; BAD REQUEST + error message on failure
     */
    @PutMapping("/{id}")
    public ResponseEntity<String> update(@PathVariable int id, @RequestBody Order order) {
        String errorMessage = orderService.update(id, order);
        ResponseEntity<String> response;
        if (errorMessage == null) {
            response = new ResponseEntity<>(HttpStatus.OK);
        } else {
            response = new ResponseEntity<>(errorMessage, HttpStatus.BAD_REQUEST);
        }
        return response;
    }
}
