package no.ntnu.gr11;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UrbanInfusionApplication {

    public static void main(String[] args) {
        SpringApplication.run(UrbanInfusionApplication.class, args);
    }

}

