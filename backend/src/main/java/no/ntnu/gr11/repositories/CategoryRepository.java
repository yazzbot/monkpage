package no.ntnu.gr11.repositories;

import no.ntnu.gr11.models.Category;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CategoryRepository extends CrudRepository<Category,Integer> {
    Category findByTitle(String title);
    List<Category>findAllByTitle(String title);
}
