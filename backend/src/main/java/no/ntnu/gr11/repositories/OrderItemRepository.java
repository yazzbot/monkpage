package no.ntnu.gr11.repositories;

import no.ntnu.gr11.models.OrderItem;
import org.springframework.data.repository.CrudRepository;

public interface OrderItemRepository extends CrudRepository<OrderItem, Integer> {
}
