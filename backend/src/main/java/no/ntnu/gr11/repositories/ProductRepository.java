package no.ntnu.gr11.repositories;

import no.ntnu.gr11.models.Product;
import no.ntnu.gr11.models.Tag;
import org.springframework.data.repository.CrudRepository;
import java.util.List;
import java.util.Set;


public interface ProductRepository extends CrudRepository<Product, Integer> {
    List<Product> findByTitle(String title);
    Product findBySlug(String slug);
    List<Product> findByTitleIgnoreCaseContaining(String title);
    List<Product> findByTagsIn(Set<Tag> tag);
}
