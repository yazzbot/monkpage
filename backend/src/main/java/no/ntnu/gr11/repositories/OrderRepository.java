package no.ntnu.gr11.repositories;

import no.ntnu.gr11.models.Order;
import org.springframework.data.repository.CrudRepository;

public interface OrderRepository extends CrudRepository<Order, Integer> {
}
