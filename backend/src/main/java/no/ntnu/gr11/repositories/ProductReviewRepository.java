package no.ntnu.gr11.repositories;


import no.ntnu.gr11.models.ProductReview;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProductReviewRepository extends CrudRepository<ProductReview, Integer> {
    List<ProductReview> findByRating(int rating);
}
