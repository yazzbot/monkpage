package no.ntnu.gr11.repositories;

import no.ntnu.gr11.models.Product;
import no.ntnu.gr11.models.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TagRepository extends JpaRepository<Tag, Integer>{
    List<Tag> findByTitleIgnoreCaseContaining(String title);
    List<Tag> findByTitle(String title);
}
