package no.ntnu.gr11.repositories;

import no.ntnu.gr11.models.Cart;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CartRepository extends CrudRepository<Cart, Integer> {
}
